# Questions génératives 

Voici une série de questions génératives que nous et d'autres collègues avons trouvées utiles pour stimuler de nouvelles connaissances et une réflexion créative dans une grande variété de situations à travers le monde. Regardez ces questions pour stimuler votre propre réflexion sur des questions liées à votre propre situation spécifique. Jouez. Faites appel à votre imagination.


## Questions pour concentrer l'attention collective sur votre situation

- Quelle question, si elle reçoit une réponse, pourrait faire la plus grande différence pour l'avenir de (votre situation spécifique) ?
- Qu'est-ce qui est important pour vous dans (votre situation spécifique) et pourquoi cela vous intéresse-t-il ?
- Qu'est-ce qui vous/nous attire dans cette enquête ?
- Quelle est notre intention ici ? Quel est le but profond (le grand "pourquoi") qui mérite vraiment nos meilleurs efforts ?
- Quelles opportunités voyez-vous dans (votre situation spécifique) ?
- Que savons-nous jusqu'à présent/qu'avons-nous encore besoin d'apprendre sur (votre situation spécifique) ?
- Quels sont les dilemmes/opportunités dans (votre situation spécifique) ?
- Quelles hypothèses devons-nous tester ou remettre en question en réfléchissant à (votre situation spécifique) ?
- Que dirait quelqu'un qui a des convictions très différentes des nôtres à propos de (votre situation spécifique) ?

## Questions pour relier les idées et approfondir la réflexion

- Qu'est-ce qui prend forme ? Qu'entendez-vous sous la variété des opinions exprimées ? Qu'y a-t-il au centre de la table ?
- Qu'est-ce qui émerge ici pour vous ? Quels nouveaux liens établissez-vous ?
- Qu'est-ce qui a eu une réelle signification pour vous dans ce que vous avez entendu ? Qu'est-ce qui vous a surpris ? Qu'est-ce qui vous a interpellé ?
- Qu'est-ce qui manque dans ce tableau jusqu'à présent ? Qu'est-ce que nous ne voyons pas ? Sur quoi devons-nous être plus clairs ?
- Quel a été votre/notre principal apprentissage, aperçu, ou découverte jusqu'à présent ?
- Quelle est la prochaine étape de réflexion que nous devons franchir ?
- S'il y avait une chose qui n'a pas encore été dite afin d'atteindre un niveau plus profond de compréhension/clarté, quelle serait-elle ?

## Les questions qui font avancer les choses

- Que faudrait-il pour créer un changement sur cette question ?
- Que pourrait-il se passer pour que vous/nous nous permettions de nous sentir pleinement engagés et énergisés par rapport à (votre situation spécifique) ?
- Qu'est-ce qui est possible ici et qui s'en soucie ? (plutôt que "Qu'est-ce qui ne va pas ici et qui est responsable ?
")
- Qu'est-ce qui nécessite notre attention immédiate pour aller de l'avant ?
- Si notre succès était totalement garanti, quelles mesures audacieuses pourrions-nous choisir ?
- Comment pouvons-nous nous soutenir mutuellement pour franchir les étapes suivantes ? Quelle contribution unique chacun de nous peut-il apporter ?
- Quels défis pourraient se présenter à nous et comment pourrions-nous les relever ?
- Quelle conversation, si elle était entamée aujourd'hui, pourrait se répercuter de manière à créer de nouvelles possibilités pour l'avenir de (votre situation) ?
- Quelle graine pourrions-nous planter ensemble aujourd'hui qui pourrait faire la plus grande différence pour l'avenir de (votre situation) ?

## source : 
L'ART DES QUESTIONS PUISSANTES 
Catalyser la perspicacité, l'innovation et l'action 
https://fr.scribd.com/document/18675626/Art-of-Powerful-Questions#



# Generative questions 

Here is a series of generative questions that we and other colleagues have found useful to stimulate new knowl-edge and creative thinking in a wide variety of situations around the world. Look at these questions to stimulate your own thinking about questions related to your own specific situation. Play. Use your imagination.


## Questions for Focusing Collective Attention on Your Situation

- What question, if answered, could make the most difference to the future of (your specific situation)?
- What's important to you about (your specific situation) and why do you care?
- What draws you/us to this inquiry?
- What's our intention here? What's the deeper pur-pose (the big “why”) that is really worthy of ourbest effort?
- What opportunities can you see in (your specificsituation)?
- What do we know so far/still need to learn about(your specific situation)?
- What are the dilemmas/opportunities in (your specific situation)?
- What assumptions do we need to test or challenge here in thinking about (your specific situation)?
- What would someone who had a very different set of beliefs than we do say about (your specific situation)?

## Questions for Connecting Ideas and Finding Deeper Insight

- What's taking shape? What are you hearing under-neath the variety of opinions being expressed?What's in the center of the table?
- What's emerging here for you? What new connections are you making?
- What had real meaning for you from what you'veheard? What surprised you? What challenged you?
- What's missing from this picture so far? What is itwe're not seeing? What do we need more clarityabout?
- What's been your/our major learning, insight, ordiscover so far?
- What's the next level of thinking we need to do?
- If there was one thing that hasn't yet been said inorder to reach a deeper level of understanding/clarity, what would that be?

## Questions That Create Forward Movement

- What would it take to create change on this issue?
- What could happen that would enable you/us tofeel fully engaged and energized about (your specific situation)?
- What's possible here and who cares? (rather than “What's wrong here and who's responsible?
”)
- What needs our immediate attention going forward?
- If our success was completely guaranteed, whatbold steps might we choose?
- How can we support each other in taking the nextsteps? What unique contribution can we eachmake?
- What challenges might come our way and howmight we meet them?
- What conversation, if begun today, could ripple outin a way that created new possibilities for thefuture of (your situation)?
- What seed might we plant together today thatcould make the most difference to the future of (your situation)?

## source : 
THE ART OF POWERFUL QUESTIONS 
Catalyzing Insight, Innovation, and Action 
https://fr.scribd.com/document/18675626/Art-of-Powerful-Questions#

